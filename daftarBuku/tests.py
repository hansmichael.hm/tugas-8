from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from django.urls import resolve
from . import views
import unittest
import time

class DaftarBukuAppUnitTest ( TestCase ) :
    def test_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    def test_app_template_is_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response,'index.html')
    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)
    


class NewVisitorTest(unittest.TestCase):
   def setUp(self):
       chrome_options = Options()
       chrome_options.add_argument('--dns-prefetch-disable')
       chrome_options.add_argument('--no-sandbox')
       chrome_options.add_argument('--headless')
       chrome_options.add_argument('disable-gpu')
       self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
       super(NewVisitorTest, self).setUp()


   def test_search_box_exist_and_result_is_shown(self):
       self.browser.get('http://localhost:8000/')
       time.sleep(1) # Let the user actually see something!
       search_box = self.browser.find_element_by_tag_name('input')
       random_keys = "Business"
       search_box.send_keys(random_keys)
       search_box.submit()
       time.sleep(1) # Let the user actually see something!
       self.assertIn(random_keys,self.browser.page_source)