# Hans' Bio

**PPW Tugas 8 Project**


## Author

* Hans Michael Nabasa Pasaribu - 1806235662

## Herokuapp Link

[Daftar Buku](https://caribuku.herokuapp.com) 

## Project Details

Web ini berfungsi untuk menampilkan daftar buku-buku yang ingin dicari berdasarkan keyword tertentu menggunakan AJAX.

## Pipelines Status

[![pipeline status](https://gitlab.com/hansmichael.hm/tugas-8/badges/master/pipeline.svg)](https://gitlab.com/hansmichael.hm/tugas-8/commits/master)

## Coverage Status
[![coverage report](https://gitlab.com/hansmichael.hm/tugas-8/badges/master/coverage.svg)](https://gitlab.com/hansmichael.hm/tugas-8/commits/master)